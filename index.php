<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INF 232</title>
</head>
<body>
    <h1 style="color: tomato"> THIS IS AN INDEX.PHP </h1>

    <?php
        //      0 1 2 3
        $arr = [1,2,3,4];

        $credentials = [
        //  $key => $value
            "name" => "Sabina",
            "surname" => "Shaganova", 
            "age" => 18,
            "student_id" => 200000,
            "gender" => "female"
        ];

        // some changes;

        foreach($credentials as $key => $value) {
            echo "<b>$key:</b> $value <br>";
        }
    ?>
</body>
</html>